package com.sda.project.services;

import com.sda.project.models.InvoiceInput;
import com.sda.project.models.ItemType;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

//TODO tema
public class FileParserServiceTest {

    private FileParserService service = new FileParserService();

    @Test
    public void testPositive() {
        //Given - un path catre fisierul cu resurse

        //getClassLoader()-stie sa imi citeasca fisierele
        //getResource() - incarca fisiere din resources
        String path = FileParserServiceTest.class
                      .getClassLoader()
                      .getResource("input.json")
                      .getPath();

        assertTrue(path.endsWith("input.json"));

        //When - apelez serviciul
        List<InvoiceInput> invoiceInputs = service.readUserFile(path);

        //Then - ar trebui sa intoarca o listw de oboecte
        assertEquals(1,invoiceInputs.size());
        assertEquals((Integer)1, invoiceInputs.get(0).getQuantity());
        assertEquals(ItemType.FILTRU_POLEN, invoiceInputs.get(0).getItemType());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegative(){
        String path = "C:\\User\\Desktop\\input.jason";
        service.readUserFile(path);
    }
}