package com.sda.project.database.dao;

import com.sda.project.models.Invoice;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class InvoiceDaoTest {
    @Mock
    private SessionFactory sessionFactoryMock;

    @Mock
    private Session sessionMock;

    @Mock
    private Transaction transactionMock;


    InvoiceDao target;

    // dau functionalitate la obiecetele mock, pt ca ele nu au nimic
    @Before
    public void setup() {

        when(sessionFactoryMock.openSession()).thenReturn(sessionMock);
        when(sessionMock.beginTransaction()).thenReturn(transactionMock);


        //intorc empty string pt ca ma obliga mockito ca sa reintorc ceva
        when(sessionMock.save(any(Invoice.class))).thenReturn("");
        //apelez commit() in afara when ului pt ca intparece  void
        doNothing().when(transactionMock).commit();

        target = new InvoiceDao(sessionFactoryMock);
    }

    @Test
    public void t1_createInvoicePositive() {
        //Given --creez un input pt salvare,consider ca nu exista nimic in db
        Invoice invoice = new Invoice();

        //When - apelez metoda createainvoicea90 din dao -care ar tb sa salveze in db
        boolean result = target.createInvoice(invoice);

        //Then - apleez cu dao un getAll din db si ma sigur ca s a salvat imututl de la apelul anterior
        assertTrue(result);
        verify(transactionMock).commit();
    }

    @Test
    public void t1_createInvoiceNegative() {

        when(sessionMock.save(any(Invoice.class))).thenAnswer(answer -> {
            throw new Exception();
        });
        doNothing().when(transactionMock).rollback();
        //Given --creez un input pt salvare,consider ca nu exista nimic in db
        Invoice invoice = new Invoice();

        //When - apelez metoda createainvoicea90 din dao -care ar tb sa salveze in db
        boolean result = target.createInvoice(invoice);

        //Then - apleez cu dao un getAll din db si ma sigur ca s a salvat imututl de la apelul anterior
        assertFalse(result);
        verify(transactionMock).rollback();
    }

    @Test
    public void t2_getInvoices() {
        //when sessionIck
        //tre sa facem in test un array list
        // whrn seesionmock create quary de any string then.list then return lista cereata mai sus
        // a 2q metoda tre sa intram pe catch, adica o sa aruncam o exceptie

        //TODO tema
    }

    @After
    public void resetDB() {
        //curata tot continututl din db, dupa fiecare test
    }
}