package com.sda.project.utils.mappers;

import com.sda.project.models.Invoice;
import com.sda.project.models.InvoiceInput;
import com.sda.project.models.ItemType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class InputToDatabaseObjectTest {

    @Test
    public void testMap() {

        //Given
        //am creat datele de input pt emtoda pe care vreau sa o testez
        InvoiceInput input = new InvoiceInput(10, ItemType.FILTRU_POLEN);

        //When
        //am apelat metoda pe care vreau sa o testez si salvez rasonsul acesteia
        Invoice result = InputToDatabaseObject.map(input);

        //Then

        assertEquals(ItemType.FILTRU_POLEN.getPrettyName(), result.getName());

        assertEquals((Integer) 10, result.getQuantity());

        Double expectedPrice = input.getQuantity() * ItemType.FILTRU_POLEN.getUnitPrice();
        assertEquals(expectedPrice, result.getTotalPrice());
        assertNotNull(result.getCreated());
    }
}