package com.sda.project;

import com.sda.project.database.configuration.HibernateConfiguration;
import com.sda.project.services.OrchestratorService;
import org.hibernate.SessionFactory;


public class Main {
    public static void main(String[] args) {
        // deschid o sesoie la inceput cue scopul se a o folosi pe parcursul rularii
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();

        OrchestratorService orchestratorService = new OrchestratorService(sessionFactory);
        orchestratorService.runAppplication();

    }
}

// (mapper)
// input.json -> Orchestratir(file parser) -> InvoiceInput class --------> Mapper -> Invoice classs -> DAO -> mySql