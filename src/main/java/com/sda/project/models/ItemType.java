package com.sda.project.models;

public enum ItemType {
    FILTRU_POLEN("Filtru Polen ", 123.3),
    ULEI_MOBIL_5W40_5L("Mobil X1 5W40 5L", 100.3);

    private final String prettyName;
    private final double unitPrice;

    ItemType(String printName, double unitPrice) {
        this.prettyName = printName;
        this.unitPrice = unitPrice;
    }

    public String getPrettyName() {
        return prettyName;
    }

    public double getUnitPrice() {
        return unitPrice;
    }
}
