package com.sda.project.models;

public class InvoiceInput {

    private Integer quantity;
    private ItemType itemType;

    public InvoiceInput( Integer quantity, ItemType itemType) {

        this.quantity = quantity;
        this.itemType = itemType;
    }

    public InvoiceInput() {
    }


    public Integer getQuantity() {
        return quantity;
    }

    public ItemType getItemType() {
        return itemType;
    }


    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }


    @Override
    public String toString() {
        return "InvoiceInput{" +
                ", quantity=" + quantity +
                ", itemType=" + itemType +
                '}';
    }
}
