package com.sda.project.services;

import com.sda.project.database.dao.InvoiceDao;
import com.sda.project.models.Invoice;
import com.sda.project.models.InvoiceInput;
import com.sda.project.utils.mappers.InputToDatabaseObject;
import org.hibernate.SessionFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class OrchestratorService {

    private final FileParserService fileParserService;
    private final InvoiceDao dao;

    public OrchestratorService(SessionFactory sessionFactory) {

        this.fileParserService = new FileParserService();
        this.dao = new InvoiceDao(sessionFactory);
    }

    public void runAppplication() {
        Scanner scanner = new Scanner(System.in);
        boolean isRunning = true;
        printMenu();

        while (isRunning) {
            int option = scanner.nextInt();
            scanner.nextLine(); // Know issue scannner

            switch (option) {
                case 0:
                    System.out.println("Thank for using the application!");
                    isRunning = false;
                    break;
                case 1:
                    System.out.print("Please input the path to file: ");
                    String path = scanner.nextLine();
                    writhContentToDatabase(path);
                    printMenu();
                    break;
                case 2:
                    printAllInvoices();
                    printMenu();
                    break;
                case 3:
                    printTodaysEntries();
                    printMenu();
                    break;
                case 4:
                    System.out.println("To be designed");
                    break;
                default:
                    printMenu();

            }
        }
    }

    private void printTodaysEntries() {
        List<Invoice> invoices = dao.getInvoices();

        //extragem data de azi, din care pastram data calendaristica
        //in format YYY-MM-DD

        String today = extractToday();

        //Tfiltram lista pe baza datei
//        invoices.stream()
//                .filter(invoice -> invoice.getCreated() != null)
//                .filter(invoice ->today.equals(invoice.getCreated().toString()))
//                .collect(Collectors.toList());

        Predicate<Invoice> isNotNullDate = invoice -> invoice.getCreated() != null;
        Predicate<Invoice> isCreatedaToday = invoice -> today.equals(invoice.getCreated().toString());
        List<Invoice> collect = invoices.stream()
                .filter(isNotNullDate)
                .filter(isCreatedaToday)
                .collect(Collectors.toList());


        printInvoices(collect);

    }

    private String extractToday() {
        // din java 8 nu mai se fol Date ci DateLocalTime
        //data poate fi fprmatata cu simpel date format ca mai jos care accepta un string pattern
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        return formatter.format(new Date());
    }

    private void printAllInvoices() {
        List<Invoice> invoices = dao.getInvoices();

        printInvoices(invoices);
    }

    private void printInvoices(List<Invoice> invoices) {
        System.out.println("You have retreaded :" + invoices.size() + "entries");
        for (int i = 0; i < invoices.size(); i++) {
            //TODO String format pad (cauta pe net) - aliniaati coloanele prin adaugarea la parametru a unui pading
            String line = String.format("%s, %s", (i + 1), invoices.get(i));
            System.out.println(line);
        }

        System.out.println("\n");
    }

    private void writhContentToDatabase(String path) {
        List<InvoiceInput> invoiceInputs = fileParserService.readUserFile(path);

        System.out.println("Successfully read " + invoiceInputs.size() + " items from path " + path);

        for (InvoiceInput inputElement : invoiceInputs) {
            //TODO - map elemet for InvoiceInput type to Invoice type
            //so we can save in database

            Invoice mappedInput = InputToDatabaseObject.map(inputElement);
            boolean isSaved = dao.createInvoice((mappedInput));
            if (!isSaved) {
                System.out.println("Could not save to database the following line " + mappedInput.toString());
            } else
                System.out.println("Succefully saved to DataBase the folloeig item " + mappedInput.toString());

        }
    }

    private static void printMenu() {
        System.out.println("Please chose an option: ");
        System.out.println("0 - Exit");
        System.out.println("1 - Input file");
        System.out.println("2 - Get all entries");
        System.out.println("3 - Get today entry");
        //TODO tema -
        System.out.println("4 - Display total income for today");
        System.out.println("5 - Show menu");
        System.out.println("\n");
    }
}

