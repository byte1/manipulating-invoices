package com.sda.project.utils.mappers;

import com.sda.project.models.Invoice;
import com.sda.project.models.InvoiceInput;

import java.util.Date;

public class InputToDatabaseObject {

    private InputToDatabaseObject() {
        throw new IllegalAccessError("Utility Class, please not instantiate");
    }

    public static Invoice map(InvoiceInput input) {
        Invoice result = new Invoice();
        result.setName(input.getItemType().getPrettyName());
        result.setQuantity(input.getQuantity());
        result.setTotalPrice(computePrice(input));
        result.setCreated(new Date());
        return result;
    }

    private static double computePrice(InvoiceInput input) {
        return input.getQuantity() * input.getItemType().getUnitPrice();
    }
}
